You enter a sparsly decoreated room with only a single chair and table.
You enter the Kitchen. A stew is still cooking on the stove and ingredients are still on the counter waiting for prep.
You enter one of the guest bedrooms. The dust makes you believe there have not been any guests here for some time.
You enter the Library. There are some engineering books open on the main table. 
You enter the Dungeon.  A couple of skeletons in the cells, cause you to want to exit this room as soon as possible.
You enter a fiery pit of doom and despair, where hopes and dream go to die. (TDQC?)
