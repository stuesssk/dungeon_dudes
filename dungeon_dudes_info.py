import random

lootList = ["Goblet of Fire", "Wand of Wonder", "Shiney Rock",
            "Gold Madalions", "Copper Cup", "Guilded Armour", "Sticky Stick",
            "Rusty Sword", "Plastic Armour", "Attack Potion", "PT Belt",
            "Half Eaten MRE", "Broken Keyboard", "Rocky Rock"]
diceSymbols = {1: "\u2680", 2: "\u2681", 3: "\u2682", 4: "\u2683",
               5: "\u2684", 6: "\u2685"}
difficultness = {0: "1) Can I Play, Daddy? (Easy)",
                 1: "2) Don't Hurt Me. (Medium)",
                 2: "3) Bring 'Em On! (Hard)",
                 3: "4) I am Death Incarnate! (Extra Carnage)",
                 4: "5) Über(Good Luck)"}


class Hero:
    """Hero class for the player to use to beat the monsters."""
    def __init__(self, playerName):
        self.name = playerName
        self.hp = 10
        self.maxHp = self.hp
        self.intiative = 0
        self.bag = []
        # Strength used to determine number of dice rolled
        self.strength = 3

    @property
    def name(self):
        return self._name

    @property
    def hp(self):
        return self._hp

    @property
    def maxHp(self):
        return self._maxHp

    @property
    def intiative(self):
        return self._intiative

    @property
    def strength(self):
        return self._strength

    @name.setter
    def name(self, name):
        self._name = name

    @hp.setter
    def hp(self, hp):
        self._hp = hp

    @maxHp.setter
    def maxHp(self, maxHp):
        self._maxHp = maxHp

    @intiative.setter
    def intiative(self, initiative):
        self._intiative = initiative

    @strength.setter
    def strength(self, strength):
        self._strength = strength

    def __str__(self):
        hero = "{0}'s HP: {1}/{2}"
        return hero.format(self.name, self.hp, self.maxHp)


class Monster:
    """Monster class, to be fought by the Big Hero."""
    def __init__(self):
        monsterNames = ['Ancalagon', 'Feanor', 'Mewlip', 'Snaga', 'Ugluk',
                        'Grinnah', 'Gothmog', 'Azog', 'Glaurung', 'Lurtz',
                        'Grishnakh', 'Yazneg', 'Sharku', 'Maeglin']
        self.name = monsterNames[random.randint(0, len(monsterNames) - 1)]
        self.hp = random.randint(1, 3)
        self.maxHp = self.hp
        # Strength used to determine number of dice rolled
        self.strength = random.randint(1, 3)
        # Monster don't carry over to other rooms have intiative
        # at initialization makes for less steps in creation
        self.initiative = random.randint(1, 6)
        self.runAway = False

    @property
    def name(self):
        return self._name

    @property
    def hp(self):
        return self._hp

    @property
    def maxHp(self):
        return self._maxHp

    @property
    def intiative(self):
        return self._intiative

    @property
    def strength(self):
        return self._strength

    @name.setter
    def name(self, name):
        self._name = name

    @hp.setter
    def hp(self, hp):
        self._hp = hp

    @maxHp.setter
    def maxHp(self, maxHp):
        self._maxHp = maxHp

    @intiative.setter
    def intiative(self, initiative):
        self._intiative = initiative

    @strength.setter
    def strength(self, strength):
        self._strength = strength

    def __str__(self):
        hero = "{0}'s HP: {1}/{2}"
        return hero.format(self.name, self.hp, self.maxHp)
