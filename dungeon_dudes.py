#!/usr/bin/env python3
import random
import time

from dungeon_dudes_info import Hero as Hero
from dungeon_dudes_info import Monster as Monster
from dungeon_dudes_info import lootList as lootList
from dungeon_dudes_info import diceSymbols as diceSymbols
from dungeon_dudes_info import difficultness as difficultness

stringBag = "1) List Contents of Bag of Holding"
stringMove = "2) Move to the Next Location"
stringHealth = "3) Display Hero's Health"
stringMonster = "4) Display Health of Monster"
stringAttack = "5) Attack Monster"
stringPick = "Player's Turn, Please Pick an Option:"
stringMenu = "\n".join((stringBag, stringMove, stringHealth, stringMonster,
                       stringAttack))
stringRun = "".join(("You have the Low ground on this attack. But your ",
                     "initiative is higher.\n",
                     "Do you want to sneak past the monster?(y/n)"))
stringError = "Input not a valid choice\u203D"
stringHealthPotion = "".join(("Would you like to use the Health pottion",
                              " to increase health by 3 hp(y/n):"))
stringAttackPotion = "".join(("Would you like to use the Attack pottion",
                              " and use one more Dice to attack(y/n):"))
stringNoLoot = "All monsters must be killed for loot to drop!"


def dieRoll():
    """Function to roll one six sided die"""
    roll = random.randint(1, 6)
    return roll


def rollDice(diceQuantity):
    """Function to roll the specified number of dice for a player or
       monster and returns the results as a list.
    """
    diceRoll = []
    # Roll the number of dice passed by diceQuantity and store the
    # Results in a list to pass back
    for dice in range(diceQuantity):
        roll = dieRoll()
        diceRoll.append(roll)
    # Return sorted in reverse, so the highest die is stored in position 0
    return sorted(diceRoll, reverse=True)


def playerOption():
    """Funtion to determine the user's next action from the list
       of menu option. Function returns the valid option picked
       by the user.
    """
    print(stringMenu)
    # Determineing what action the user wants to take next and
    # Ensuring the action is a valid option from the menu
    while True:
        try:
            action = int(input(stringPick))
            if action < 6 and action > 0:
                return action
            raise ValueError
        except(ValueError):
            print(stringError)


def roomImport():
    """Function used to intialize the room descriptions list"""
    fileHandle = open("Rooms.txt", "r")
    # Readlines store each line as a member of the list
    lines = fileHandle.readlines()
    # Be nice and close the file
    fileHandle.close()
    return lines


def difficultyLevel():
    """Function used to get the difficulty the user wishes to play
       the game.
    """
    # Print the menu options for difficulty
    print("Difficulty:")
    for index in range(len(difficultness)):
        print(difficultness.get(index))
    # Ensure the user only typed one of the menu numbers
    while True:
        difficulty = input("Please select a difficulty:")
        try:
            difficulty = int(difficulty)
            if difficulty > 0 and difficulty < 6:
                return difficulty
            else:
                raise ValueError
        except(ValueError):
            print(stringError)


def isHero(player):
    """Function to determine if the player is the attacker or defender,
       once the monsters and player get passed to a function and their
       identities may be unknown.
    """
    # Testing for the bag attribute, which is only attributed to the
    # hero class
    test = getattr(player, "bag", None)
    return test


def potionInBag(player):
    if "Health Potion" in player.bag:
        useHealthPotion(player)


def attack(attacker, defender):
    """Function for combat mechanics. Initative determines attacker
       and defneder (lower intiative is defending). Ties go to
    """
    print("Attacking:", attacker)
    print("Defending:", defender)
    pauseGame()
    # If player is attacker, has option to sneak
    # and checking if health potion is available
    test = isHero(attacker)
    if test is not None:
        # Player has higher intiative give them the ability to sneaking
        # sneak around the enemy
        if bypass(defender):
            print("You succede in sneaking past the monster.")
            return
        potionInBag(attacker)
    # Is defender the player and is Health potion in bag
    test = isHero(defender)
    if test is not None:
        potionInBag(defender)

    # The combat. Two enter only one can exit
    while attacker.hp > 0 and defender.hp > 0:
        attackDice = rollDice(attacker.strength)
        defendDice = rollDice(defender.strength)
        # Show the attacker's dice roll
        print(attacker.name, "'s", " Roll:", sep="")
        for dice in attackDice:
            print(diceSymbols.get(int(dice)), end=" ")
        print("")
        # Show the defender dice roll
        print(defender.name, "'s", " Roll:", sep="")
        for dice in defendDice:
            print(diceSymbols.get(int(dice)), end=" ")
        print("")
        # Whoever has the higher die wins the battle and
        # the other loses a point of health. Ties go to attacker,
        # whomever had the higher intiative when the battle began.
        if attackDice[0] >= defendDice[0]:
            print(defender.name, "Loses Roll, -1 Health")
            defender.hp -= 1
        else:
            print(attacker.name, "Loses Roll, -1 Health")
            attacker.hp -= 1
        # Give the user some time to absorb what happened during the roll
        time.sleep(1)


def bypass(defender):
    """Fucntion to allow the user to sneak past an enemy if they have the
       higher intiative
    """
    # Asking if they want to runaway
    runaway = input(stringRun)
    while True:
        runaway = runaway.lower()
        # Ensuring the user input a y or n, case dows not matter
        if runaway == "y" or runaway == "n":
            break
        else:
            runaway = input("Please input y or n:")
    # If user has chossen to run away set the falg in the moster
    # so the user can still exit the room with out fighting that monster
    if runaway == "y":
        defender.runAway = True
    return defender.runAway


def pauseGame():
    """Function used to pause the game to allow the player to readline
       text displayed to the screen.
    """
    # Pause the game to allow the player to digest what has been
    # diplayed on the sceen
    input("Press Return to Continue...")


def enterRoom(rooms, enemies):
    """Function to spawn monsters when the hero enters the room"""
    # Spawn the Monsters determined by difficulty selected in beginging
    # of game
    numMonster = random.randint(1, enemies)
    monsters = []
    # Append each new monster to the monsters list and return the list
    # when every monster is created
    for enemies in range(numMonster):
        monster = Monster()
        monsters.append(monster)

    # Hero enters a new
    print(rooms[random.randint(0, len(rooms) - 1)])
    print("The room has", len(monsters), "monster!\n")
    return monsters


def roomCleared(monsters):
    """Function to check if all the monsters have been defeated in
       the roomCleared
    """
    # Room is cleared, if for each monster the player has defeated
    # it or managed to bypass the monster by sneaking around it
    for monster in monsters:
        if monster.hp > 0 and not monster.runAway:
            print("Monster blocking your path.  He must be defeated",
                  "before you can continue\n")
            return False
        elif monster == monsters[-1]:
            return True


def printBag(bag):
    """Function to print the players bag of holding"""
    print("\n", stringBag[8:], ":", sep="")
    # Cycle through each item in the Bag of Holding and print it
    for items in bag:
        print(items)
    if len(bag) == 0:
        # If bag is empty, let the player know instead of printng nothing
        # to the screen
        print("Bag of Holding is currently Empty\n")
    print("")


def useHealthPotion(player):
    """Fucntion to allow the user to use their health Potion to increase
       their health by 3
    """
    # Asking if they want to runaway
    useHealth = input(stringHealthPotion)
    while True:
        useHealth = useHealth.lower()
        # Ensuring the user input a y or n, case dows not matter
        if useHealth == "y" or useHealth == "n":
            break
        else:
            runaway = input("Please input y or n:")
    # If chosen add 3 hp to health
    if useHealth == "y":
        player.hp += 3
        print("Health Potion used")
        player.bag.remove("Health Potion")
        print(player)


def lootRoom(player, monsterKills, monsterCount):
    """Function to give the players loot if all monsters in the
       room have been killed. No loot if any monsters were avoided.
    """
    if monsterKills == monsterCount:
        # If all monsters are killed give the player random loot
        # One loot per monster killed(Increase chance for attack potion).
        for number in range(0, monsterKills):
            player.bag.append(lootList[random.randint(0, len(lootList) - 1)])
            # Attack potion can only have one occurance per game
            if attackInBag(player):
                lootList.remove("Attack Potion")
            # 1/3 chance a health potion drops in the loot
            if "Health Potion" not in player.bag:
                # Health potion can drop if not in bag
                # gives hero a fighting chance on higher difficulties
                if random.randint(1, 10) < 3:
                    player.bag.append("Health Potion")
        # Display the contents of the bag of holding for the player
        printBag(player.bag)
    else:
        # All monsters must be killed for loot to drop
        print(stringNoLoot)


def readFile(textFile):
    """Function to read in the contents of a file line by line and display
       the results on the screen
    """
    # Read and display the text file passed to the function
    # File closed automatically
    with open(textFile, "r") as fileHandle:
        while True:
            text = fileHandle.readline()
            if not text:
                break
            # Don't need the newlione since print will print a newline
            # by default
            print(text.rstrip())
    print("\n")


def attackInBag(player):
    """Funtion to determine if Attack potion is in the Player's
       Bag  of Holding.
    """
    test = False
    # Checking if attack potion is in the bag of holding
    if "Attack Potion" in player.bag:
        test = True
    return test


def useAttackPotion(player):
    """Function to use the Attack Potion if the player wants to"""
    useAttack = input(stringAttackPotion)
    while True:
        useAttack = useAttack.lower()
        # Ensuring the user input a y or n, case does not matter
        if useAttack == "y" or useAttack == "n":
            break
        else:
            useAttack = input("Please input y or n:")
    # If chosen add 3 hp to health
    if useAttack == "y":
        player.strength += 1
        print("Attack Potion used")
        player.bag.remove("Attack Potion")
        print(player)


def inventory(player, monsters):
    """Function for inventory menuDict option"""
    # Functoin call to print the players Bag of Holding
    printBag(player.bag)


def move(player, monsters):
    """Function for move menuDict option"""
    global rooms
    global howHard
    # If the room is cleared( all monsters killed or avoided)
    # Go ahead and move to next room
    if roomCleared(monsters):
        # If moving to new room, populate new roomCleared
        # and reroll hero's initiative
        print("\nMoving", stringMove[8:])
        return True


def health(player, monsters):
    """Function for health menuDict option"""
    # Print the health of the hero
    print("")
    print(stringHealth[11:])
    print(player)
    print("")


def monsterHealth(player, monsters):
    """Function for monsterHealth menuDict option"""
    # Print the health of the monsters in the room
    print("")
    print(stringMonster[11:])
    for monster in monsters:
        print(monster)
    print("")


def attacking(player, monsters):
    """Function for attacking menuDict option"""
    global roomCount

    # Checking if attack potion is in bag and if player
    # wantsd to use the potion for an extra die during combat
    if attackInBag(player):
        useAttackPotion(player)
    # Check in case user picks to battle monsters
    # when all monsters have been defeated in the room
    if monsters[0].hp == 0:
        print("All monsters in room have been defeated!\n")
        return
    # Loot only drops if all monsters are killed
    monsterKills = 0

    for monster in monsters:
        # Highest intiative is the attacker
        if player.initiative > monster.initiative:
            attack(player, monster)
        else:
            attack(monster, player)
        # Testing to ensure hero is still alive
        if player.hp == 0:
            readFile("lose.txt")
            exit()
        if monster.hp == 0:
            monsterKills += 1
        # Ensure strength is reset to normal if attack potion
        # is ever used
        player.strength = 3

    lootRoom(player, monsterKills, len(monsters))
    roomCount += 1


def defaultFunction(player, monsters):
    """Function to default to if player's choice is not part of menuDict"""
    print("")
    print(stringError)


# Dictionary for Player's in game options. Must be declared after the
# functions it will try to call
menuDict = {"1": inventory, "2": move, "3": health, "4": monsterHealth,
            "5": attacking}


def main():
    """Main function of the program. Player is craeted and tries to
       complete the adventure.
    """
    global roomCount
    global rooms
    global howHard
    readFile("title.txt")
    howHard = difficultyLevel()
    winCount = 5 + howHard
    # Spawn the Hero
    playerName = input("Please input your name:")
    playerName = " ".join(("Engineer", playerName))
    player = Hero(playerName)
    # Display Opening text
    readFile("story.txt")
    # Allow player to read opening text
    pauseGame()
    # import room descriptions as a list
    rooms = roomImport()
    roomCount = 0
    monsters = enterRoom(rooms, howHard)
    # Roll for initial initiative
    player.initiative = dieRoll()

    #  Time to play the game
    while True:
        print(stringMenu)
        choice = input(stringPick)
        # Using dictionary to invoke the functions to play the game
        # If choice is not in dict the defaultFunction is called and
        # menu is displayed again for valid choice
        menureturn = menuDict.get(choice, defaultFunction)(player, monsters)
        if menureturn:
            monsters = enterRoom(rooms, howHard)
            player.initiative = dieRoll()
        if roomCount == winCount:
            readFile("win.txt")
            exit()


if __name__ == "__main__":
    main()
